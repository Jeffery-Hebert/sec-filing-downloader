"""A quick and brief module for downloading SEC filings. """

# import json
import os
import requests


API_KEY = ""
API_ENDPOINT = "https://api.sec-api.io"
HEADERS = {"Content-type": "application/json", "Authorization": API_KEY}


def get_filings(ticker, filing_type, max_filings):
    """Need to grab links to the filings to be able to download them."""
    query = {
        "query": {
            "query_string": {
                "query": f"ticker:{ticker} AND formType: {filing_type}",
                "time_zone": "America/New_York",
            }
        },
        "from": "0",
        "size": max_filings,
        "sort": [{"filedAt": {"order": "desc"}}],
    }
    response = requests.post(API_ENDPOINT, headers=HEADERS, json=query, timeout=3)
    filings = response.json()["filings"]

    print(f"Found {len(filings)} {filing_type} filings for ticker: {ticker}")
    return filings


def download_filings(filings, output_dir):
    """."""
    for filing in filings:
        url = "https://api.sec-api.io/filing-reader"
        params = {
            "url": filing["documentFormatFiles"][0]["documentUrl"],
            "type": "pdf",
            "token": API_KEY,
        }
        response = requests.get(url, params=params, timeout=5)
        if response.status_code != 200:
            print(f"Failed to get file: {filing['linkToHtml']}")
            continue
        date = filing["filedAt"].replace("-", "")
        date = date[0:8]
        file_name = f"{filing['ticker']} {date}"
        file_path = os.path.join(output_dir, f"{file_name}.pdf")
        with open(file_path, "wb") as file:
            file.write(response.content)


def main():
    """."""
    ticker = input("Please input the ticker: ")
    filing_type = input("Please input the desired filing: ")
    max_filings = input("Please input the maximum number of files returned: ")
    output_dir = "filings"
    os.makedirs(output_dir, exist_ok=True)
    filings = get_filings(ticker, filing_type, max_filings)
    download_filings(filings, output_dir)
    print("Done")


if __name__ == "__main__":
    main()
